/*******************************************************************************
 * Copyright(c) 2017, Volansys Technologies                                    *
 *                                                                             *
 * This is a chat_server file.                                                 * 
 *                                                                             *
 * This is a chat_server file which will call the APIS of server side.         *
 * The API is called in this file and function will search defination from     *
 * server.h.                                                                   *
 ******************************************************************************/

/**
 * @file chat_server.c
 * @author Priyal Mehta
 * @date 09/11/2017
 * @brief
 *
 * chat_server file which called server APIS.
 *
 * This file allowed UDP and TCP connection which will use to communication between 
 * server and client. The features of the API is login request, logout request, send,
 * help. First receive message and then check with data base and tnen send acknowledgement 
 * to client.
 */

/************************
 * Include
************************/
#include "header.h"
#include "server.h"

/***********************
 * Function Prototypes
 ***********************/
/**
 * @brief This is a main function.
 *
 * The function main() is a main function of file. main() indicate complier to
 * start program execution from main(). Here main function is designed to use
 * different APIs from library  which is establish the connetion. main() will
 * first initilize socket and then receive message from client and check username
 * and password with database and then send acknowledgement to client and update fd 
 * and status of client in database.
 *
 */

int main(int argc, char *argv[]){

	/** variable initilization */
	socklen_t len;
	struct sockaddr_in server;
	int fd = 0;
	struct sockaddr_in client;
	int new_sock_desc = 0;
	struct thread_args args;
	app_ret_t ret;
	pthread_t thread1;

	/** Initilize mutex */
	pthread_mutex_init(&(args.mutex), NULL);
	
	/** Fill memory with constant zero. */
	memset(&server,0,sizeof(server));
	memset(&client,0,sizeof(client));

	/** Socket initilization */
	ret = socket_server_init(&server, &fd);
	if(ret == FAIL){
		debug_print("Error in socket connection with ret val %d\n", ret);
		return FAIL;
	}
	load_database();
	display_database();

	while(1){   
#ifdef TCP
	        len = sizeof(client);  
		/** Accept new client request */
		new_sock_desc = accept(fd, (struct sockaddr *)&client, &len);
		if(new_sock_desc == FAIL){
			debug_print("Error in temporary socket creation !!\n");
			return FAIL;
		}

		/** Lock mutex */
		pthread_mutex_lock(&(args.mutex));

		/** Fd store in structure */
		args.new_sock_desc = new_sock_desc;
#else
		args.new_sock_desc = fd;
#endif
		args.client = client;

		/** Unlock mutex */
		pthread_mutex_unlock(&(args.mutex));

		printf("Connection accepted... !!\n\n");
	
		/** Thread creat and pass structure with fd */
		if(pthread_create(&thread1, NULL, handle_new_client,(void *)&args) < 0){
			debug_print("Error in thread create !!\n");
			return FAIL;
		}
	}
	/** free database */
	destroy();
	return NO_ERROR;
}

