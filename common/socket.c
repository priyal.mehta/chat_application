#include<stdio.h>
#include"common.h"

app_ret_t socket_server_init(struct sockaddr_in *server, int *fd){

	static int sock_desc = 0;
	int op = TRUE;
	app_ret_t ret;

#ifdef TCP
        if((sock_desc = socket(AF_INET, SOCK_STREAM, 0)) == -1){
                debug_print("Error in socket creation !!\n");
                return FAIL;
        }
#else
	if((sock_desc = socket(AF_INET, SOCK_DGRAM, 0)) == -1){
                debug_print("Error in socket creation !!\n");
                return FAIL;
        }
#endif

        server->sin_family = AF_INET;
        server->sin_addr.s_addr = INADDR_ANY;
        server->sin_port = htons(PORT);

#ifdef TCP	
	if(setsockopt(sock_desc, SOL_SOCKET, SO_REUSEADDR, (char *)&op,sizeof(op)) < 0 ){  
		debug_print("Error in set socket option !!\n");
                return FAIL;
	} 
#endif
	
        ret = bind(sock_desc,(struct sockaddr *)server, sizeof(struct sockaddr));
	if(ret == FAIL){
		debug_print("Error in binding !!\n");
                return FAIL;
	}

#ifdef TCP
	ret = listen(sock_desc,20);
	if(ret == FAIL){
                debug_print("Error in listening !!\n");
                return FAIL;
	}
#endif

	*fd = sock_desc;
        printf("\nTCP / UDP Server Waiting for client on port 3004 !!\n");

	return NO_ERROR;
}

app_ret_t socket_client_init(struct sockaddr_in *client, int *fd){

	int sock_desc = 0;
	app_ret_t ret;

#ifdef TCP
	if ((sock_desc = socket(AF_INET, SOCK_STREAM, 0)) == FAIL) {
                debug_print("Error in socket creation with ret val : %d\n",ret);
                return FAIL;
        }
#else
        if ((sock_desc = socket(AF_INET, SOCK_DGRAM, 0)) == FAIL) {
                debug_print("Error in socket creation with ret val : %d\n",ret);
                return FAIL;
        }

#endif
        client->sin_family = AF_INET;
        client->sin_addr.s_addr = inet_addr(INET);
        client->sin_port = htons(PORT);

#ifdef TCP
	ret = connect(sock_desc, (struct sockaddr*)client, sizeof(struct sockaddr));
        if(ret == FAIL){
		debug_print("Error in connection with ret val : %d\n",ret);
		return FAIL;
	}
#endif
        
	*fd = sock_desc;
	return NO_ERROR;
}

app_ret_t socket_send(int *sock_desc, char *message,struct sockaddr_in *server){

	socklen_t slen = sizeof(*server);
        app_ret_t ret;

#ifdef UDP
	ret = sendto(*sock_desc, message, MAX_MESSAGE_SIZE, 0, (struct sockaddr *)server, slen);
        if(ret <= 0){
		debug_print("Error in send message with ret val : %d\n", ret);
                perror("send server failed !!:");
                return FAIL;
	}

#else
	ret = send(*sock_desc, message, MAX_MESSAGE_SIZE, 0);
	printf("return = %d\n",ret);
	if(ret <= 0){
		debug_print("Error in sending message with ret val : %d\n", ret);
                perror("send server failed !!:");
                return FAIL;
        }
#endif
        return NO_ERROR;
}


app_ret_t socket_receive(int *sock_desc, char *message, struct sockaddr_in *server){

	socklen_t slen = sizeof(*server);
        app_ret_t ret;

        #ifdef UDP
                ret = recvfrom(*sock_desc, message, MAX_MESSAGE_SIZE, 0, (struct sockaddr *)server, &slen);
                if(ret <= 0){
                        debug_print("Error in receiving message with ret val : %d\n", ret);
                        perror("receive server failed !!:");
                        return FAIL;
                }

        #else
                ret = recv(*sock_desc, message, MAX_MESSAGE_SIZE, 0);
                printf("return = %d\n",ret);
                if(ret <= 0){
                        debug_print("Error in receiving message with ret val : %d\n", ret);
                        perror("receive server failed !!:");
                        return FAIL;
                }
        #endif
        return NO_ERROR;
}

