/************************
 * Include
************************/
#include<stdio.h>
#include<stdlib.h>
#include<pthread.h>
#include<unistd.h>
#include<sys/types.h>
#include<stdint.h>
#include<errno.h>
#include<sys/socket.h>
#include<signal.h>
#include<netinet/in.h>
#include<arpa/inet.h>
#include<string.h>
#include<netdb.h>
#include<getopt.h>
#include"header.h"


/************************
 * Macro
************************/
#define UDP 1
#define TCP 0

/************************
 * Enum
************************/
typedef enum ret_t{
	NO_ERROR = 0,
	NULL_POINTER,
	ERR_CLIENT_INIT,
	ERR_INVALID_LOGIN_DETAILS,
	ERR_SENDING_MSG,
	ERR_AUTHENTICATION,
	ERR_USER_EXISTS,
	ERROR_INVALID_FD,
	ERR_INVALID_PASSWORD,
	FAIL = -1
}app_ret_t;

/************************
 * Enum
************************/
typedef enum status{
	OFFLINE,
        ONLINE
}app_status;

/************************
 * Enum
************************/
typedef enum frame_type{
        LOGIN_REQ,
	LOGIN_RESPONSE,
        LOGOUT_REQ,
	LOGOUT_RESPONSE,
        MSG,

}app_frame_type;

/************************
 * Enum
************************/
typedef enum login_response{
        LOGIN_RESPONSE_SUCESSFULLY_LOGIN,
	LOGIN_RESPONSE_USERNAME_INVALID,
        LOGIN_RESPONSE_PASSWORD_INVALID,
        LOGIN_RESPONSE_USER_EXISTS,
}login_response_t;


/************************
 * Define
************************/
#ifdef DEBUG
	#define debug_print(fmt, args...) fprintf(stderr, "%s:%d:%s(): " fmt, __FILE__,__LINE__, __FUNCTION__, ##args);
#else
        #define debug_print(...)
#endif

#define INET "127.0.0.1"
#define PORT 3004

/************************
 * Function Prototype
************************/
/**
 * @brief To display the list on screen.
 *
 * The function is used to display the  list data. For this function will 
 * travel to the end of the list and print the data for each node.
 *
 */
app_ret_t display_database();

/**
 * @brief This function is load data from the file.
 *
 * This function is read file and then copy data particular node structure.
 * And initailly client status is offline and client fd is 0.
 *
 */
app_ret_t load_database();

/** 
 * @brief initilize the socket 
 * 
 * This function is used to create socket id and then bind and listen socket.
 *
 * @param[in]	struct sockaddr_in *client	socket address structure
 *
 * @param[out]	*fd				socket fd
 *
 */
app_ret_t socket_server_init(struct sockaddr_in *client, int *fd);

/**
 * @brief initilize the socket 
 * 
 * This function is used to create socket id and then connect socket.
 *
 * @param[in]	struct sockaddr_in *client	socket address structure
 *
 * @param[out]	*fd				socket fd
 *
 */
app_ret_t socket_client_init(struct sockaddr_in *client, int *fd);

/**
 * @brief This function is receive message
 *
 * This function is receive message on socket. In this function receivefrom is used
 * for UDP and recv is used for TCP.
 *
 * @param[in]	*sock_desc			socke fd
 *
 * @param[in]	message				receive message
 *
 * @param[in]	struct sockaddr_in *server	socket address structure
 *
 */
app_ret_t socket_receive(int *sock_desc,char *message,  struct sockaddr_in *server);

/**
 * @brief This function is send message
 *
 * This function is send message on socket. In this function sendfrom is used
 * for UDP and send is used for TCP.
 *
 * @param[in]   *sock_desc                      socke fd
 *
 * @param[in]   message                         receive message
 *
 * @param[in]   struct sockaddr_in *server	socket address structure
 *
 */
app_ret_t socket_send(int *sock_desc,char *message, struct sockaddr_in *server);
