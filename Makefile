CC = gcc
CFLAGS = -Iclient/include -Wall -Icommon/include -Iserver/include 
RM = rm -rf
CHAT_CLIENT = chat_client
CHAT_SERVER = chat_server

SUBDIRS = . client common server

SUBDIRSCLEAN = $(addsuffix clean,$(SUBDIRS))

all :  	$(CHAT_CLIENT) $(CHAT_SERVER) $(CHAT_CLIENT_1)
	$(MAKE) -C server 
	$(MAKE) -C common
	$(MAKE) -C client

$(CHAT_CLIENT) : $(CHAT_CLIENT).o libclient.a libcommon.a
	$(CC) -lm -pthread -o $(CHAT_CLIENT) $(CHAT_CLIENT).o -L./client -lclient -L./common -lcommon -g

$(CHAT_CLIENT).o : $(CHAT_CLIENT).c  

$(CHAT_CLIENT_1).o : $(CHAT_CLIENT_1).c  

$(CHAT_SERVER) : $(CHAT_SERVER).o libserver.a libcommon.a
	$(CC) -lm -pthread -o $(CHAT_SERVER) $(CHAT_SERVER).o -L./server -lserver -L./common -lcommon -g

$(CHAT_SERVER).o : $(CHAT_SERVER).c

libclient.a: 
	$(MAKE) -C client
libserver.a :
	$(MAKE) -C server
libcommon.a :
	$(MAKE) -C common

clean : $(SUBDIRSCLEAN)

clean_curdir:
	rm -rfv  $(CHAT_CLIENT) $(CHAT_SERVER) *.o *.a 

%clean: %
	$(MAKE) -C $< -f $(PWD)/Makefile clean_curdir



