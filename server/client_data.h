/************************
 * Include
************************/
#include<stdio.h>

/************************
 * Structure
************************/
struct user_details{
        char username[MAX_USERNAME_SIZE + 1];
        char password[MAX_PASSWORD_SIZE + 1];
        int fd;
        app_status status;
        struct user_details *next;
};

/************************
 * Function Prototype
************************/

/**
 * @brief This function is used to compare login data for client.
 *
 * This function is compare username and password with database and then 
 * store fd and status in database.
 *
 * @param[in]	*username		pass username
 * 
 * @param[in]	*password		pass password
 * 
 * @param[in]	*fd			pass fd
 *
 * @param[out]	*response 		return respose
 *
 */ 
app_ret_t authenticate_client(char *username, char *password, int *fd, int *response);

/**
 * @brief This function is used to get fd for other client.
 *
 * This function is compare destination username and check status if client is online
 * then get fd.
 *
 * @param[in]   *dest_username          pass dest_username
 * 
 * @param[out]   *fd                     pass fd
 *
 */
app_ret_t get_fd_for_client(char *dest_username, int *fd);

/**
 * @brief This function is used when user want to logout.
 *
 * This function is used when user send logout command then serve receive
 * message and change in database to fd and status.
 * 
 * @param[in]   *fd                     pass fd
 *
 */
app_ret_t logout_handler(int *fd);

