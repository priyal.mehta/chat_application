/************************
 * Include
************************/
#include "server.h"
#include "client_data.h"


app_ret_t login_req_handler(char *msg, struct thread_args args){

	/** Variable Declaration */
        char username[MAX_USERNAME_SIZE];
        char password[MAX_PASSWORD_SIZE];
        char message[MAX_MESSAGE_SIZE];
        app_ret_t ret;
        int response;

	/** Copy userame from frame */
        memcpy(username, msg + LOGIN_REQ_USERNAME_START, MAX_USERNAME_SIZE);

	/** Copy password from frame */
        memcpy(password, msg + LOGIN_REQ_PASSWORD_START, MAX_PASSWORD_SIZE);

	/** compare username and password with database */
        ret = authenticate_client(username, password, &(args.new_sock_desc), &response);                   
        if(ret != NO_ERROR){
                debug_print("Error in Login with ret val %d\n", ret);
                return FAIL;
        }
        message[HEADER_CMD_INDEX] = LOGIN_RESPONSE;
        message[MESSAGE_RESPONSE] = response;

	/** Send message */
        ret = socket_send(&(args.new_sock_desc), message,  &(args.client));
        if(ret == FAIL){
                debug_print("Error in sending Message with ret val %d\n", ret);
                return FAIL;
        }
        return NO_ERROR;
}

app_ret_t server_handler(char *message, struct thread_args args){

	/** Variable declaration */
        app_ret_t ret;
        app_frame_type check;
	
	/** check message header */
        check = message[HEADER_CMD_INDEX];
        switch(check){
                case LOGIN_REQ:
                        ret = login_req_handler(message, args);
                        if(ret != NO_ERROR){
                                debug_print("Error in login request handler with ret val %d\n", ret);
                                return NO_ERROR;
                        }
                        break;

                case LOGOUT_REQ:
                        ret = logout_handler(&(args.new_sock_desc));
                        if(ret != NO_ERROR){
                                debug_print("Error in logout handler with ret val %d\n", ret);
                                return NO_ERROR;
                        }
                        printf("Thank You !!\n");
                        pthread_exit(NULL);
                        break;

                case MSG:
                        ret = message_handler(message, args);
                        if(ret != NO_ERROR){
                                debug_print("Error in message handler with ret val %d\n", ret);
                                return NO_ERROR;
                        }
                        break;

                default:
                        printf("Invalid Message type !!\n");
                        break;
                }
        return NO_ERROR;
}

app_ret_t message_handler(char *msg, struct thread_args args){

	/** Variable declaration */
        char dest_username[MAX_PASSWORD_SIZE];
        char message[MAX_MESSAGE_SIZE];
        int fd = 0;
        app_ret_t ret;

	/** fill memory with constant zero */
        memset(dest_username, 0, MAX_USERNAME_SIZE);

	/** Copy dest_username from frame */
        memcpy(dest_username, msg + MSG_DEST_USERNAME_START, MAX_USERNAME_SIZE);

	/** Find fd of dest_username */
        ret = get_fd_for_client(dest_username, &fd);
	if(ret != NO_ERROR){
		debug_print("Error in get fd for client with ret val %d\n", ret);
	}

	/** fill message in header */
        message[HEADER_CMD_INDEX] = MSG;
        memcpy((message + MSG_START), msg + MSG_DATA, (MAX_MESSAGE_SIZE - MAX_USERNAME_SIZE - MAX_USERNAME_SIZE - HEADER_CMD_SIZE));
	
	/** Send message */
        ret = socket_send(&fd, message,(&(args.client)));
        if(ret == FAIL){
                debug_print("Error in sending with ret val %d\n", ret);
                return FAIL;
        }
        return NO_ERROR;
}

void *handle_new_client(void *thread){

	/** Variable declaration */
        struct thread_args args;
        app_ret_t ret;
        char *receive_frame = (char *)malloc(MAX_MESSAGE_SIZE * sizeof(char));
        int errno;

	/** mutex lock */
	pthread_mutex_lock((&((struct thread_args *)thread)->mutex));
	/** copy data */
	memcpy(&args, thread, sizeof(struct thread_args));
	/** mutex unlock */
	pthread_mutex_unlock(&(((struct thread_args *)thread)->mutex));

        while(1){
		
		/** Receive message */
		ret = socket_receive(&(args.new_sock_desc),receive_frame, (&(args.client)));
                if(ret == FAIL){
                        debug_print("Error in receiving Message with ret val %d\n", ret);
                        free(receive_frame);
                        break;
                }

                ret  = server_handler(receive_frame, args);
                if(ret == FAIL){
                        debug_print("Error in server handler with ret val %d\n", ret);
                }
		
        }
	/** close socket */
        close(args.new_sock_desc);
	return NO_ERROR;
}

