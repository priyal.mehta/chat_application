/************************
 * Include
************************/
#include"server.h"
#include"client_data.h"

/************************
 * Define
************************/
#define FILE_SIZE 1024
#define FILE_NAME "app_database.txt"

/************************
 * Globle variable
************************/
struct user_details *head;

app_ret_t display_database(){

	/** Variable declaration */
        struct user_details *new_node;

	/** Assign head other node */
        new_node = head;
        printf("*********************************************\n");
        printf("*** username    password    fd    status  ***\n");

	/** Print data of node */
        if (new_node->next!=NULL)
                while(new_node -> next != NULL) {
                        printf("*** %s    %s    %2d    %s ***\n",new_node->username,new_node->password,new_node->fd,new_node->status == OFFLINE ? "OFFLINE":"ONLINE");
                        new_node = new_node->next;
                }
        printf("*********************************************\n");
        return NO_ERROR;
}

app_ret_t load_database(){

	/** Variable declaration */
        struct user_details *new_node;
        FILE *fp;
        char *database;
        database = malloc(sizeof(char)* FILE_SIZE);
        size_t size = FILE_SIZE;
        fp = fopen(FILE_NAME,"r");

	/** Memory Allocation */
        head = malloc(sizeof(struct user_details));
	if(head == NULL){
		debug_print("Error in memory allocation !!\n");
		return NULL_POINTER;
	}

        new_node = head;

	/** Get data from file */
        while(getline(&database, &size, fp) > 0){

		/** Copy username to node */
                strncpy (new_node->username , database, MAX_USERNAME_SIZE);
                new_node->username[MAX_USERNAME_SIZE]='\0';
                database =  database + 9;
		/** Copy password to node */
                strncpy (new_node->password , database, MAX_PASSWORD_SIZE);
                new_node -> fd = 0;
                new_node -> status = OFFLINE;
                new_node -> next = malloc (sizeof(struct user_details));
                new_node =  new_node -> next;

        }
        return  NO_ERROR;
}

app_ret_t authenticate_client(char *username, char *password, int *fd, int *response){

	/** Variable declaration */
        struct user_details *search;
        search = head;

        while(search != NULL){
		/** Compare username with list */
                if((strncmp(search->username, username, MAX_USERNAME_SIZE) == 0)){
			/** Compare Password with list */
                        if(strncmp(search->password, password, MAX_PASSWORD_SIZE) == 0){
                                if(search->status == OFFLINE){
					/** Change status and fd in database */
                                        search->status = ONLINE;
                                        search->fd = *fd;
					*response = LOGIN_RESPONSE_SUCESSFULLY_LOGIN;
					return NO_ERROR;
                                }
                                else{
					*response = LOGIN_RESPONSE_USER_EXISTS;
					return ERR_USER_EXISTS;
                                }
                        }
                        else{
				*response = LOGIN_RESPONSE_PASSWORD_INVALID;
				return ERR_AUTHENTICATION;
                        }
                }
                else{
                	search = search->next;
                }
        }
        return NO_ERROR;
}


app_ret_t get_fd_for_client(char *dest_username, int *fd){

	/** Variable declaration */
	struct user_details *search;
        search = head;

	while(search != NULL){
		/** Compare destination username with list and status also */
                if((strncmp(search->username, dest_username, MAX_USERNAME_SIZE) == 0)){
			if(search->status == ONLINE){
				*fd = search->fd;
				return NO_ERROR;
			}
			else{
				return FAIL;
			}
		}
		else{
			search = search->next;
		}
	}
	return NO_ERROR;
}

app_ret_t destroy(){
	/** Variable Declaration */
	struct user_details *temp;

        temp = head;
        head = head->next;

        while(head != NULL){
		/** free Memory */
                free(temp);
                temp = head;
                (head) = (head)->next;
        }
        return NO_ERROR;

}

app_ret_t logout_handler(int *fd){
	
	/** Variable Declaration */
	struct user_details *search;
	search = head;

	while(search != NULL){
		/** fd comapare with database and change status */
		if(search->fd == *fd){
			if(search->status == ONLINE){
				search->status = OFFLINE;
				search->fd = 0;
				return NO_ERROR;
			}
		}
		else{
			search = search->next;
		}
	}
	return ERROR_INVALID_FD;
}
