/************************
 * Include
************************/
#include"common.h"

/************************
 * Structure
************************/
struct thread_args{
        int new_sock_desc;
        struct sockaddr_in client;
	pthread_mutex_t mutex;
};

/************************
 * Function Prototype
************************/
/** 
 * @brief This function free list.
 *
 * This fuction is free database upto last node.
 *
 */
app_ret_t destroy();

/**
 * @brief This function handle new client.
 * 
 * This function receive message and then pass to server handler with message.
 *
 * @param[in]	*new_desc		pass fd
 *
 */
void  *handle_new_client(void *new_desc);

/** 
 * @brief This function send message to other client.
 *
 * This message receive dest_username with message and then check status of
 * dest_username is online then fill fd and return and then message send to
 * new fd.
 * 
 * @param[in]	*msg			pass message
 *
 * @param[in]	stuct thread_args	pass structure
 *
 */
app_ret_t message_handler(char *msg, struct thread_args args);

/**
 * @brief This functon is receive mesage and take deciseon.
 * 
 * This function is check message header and then check in switch case and then 
 * particular function call.
 *
 * @param[in]	*message		pass message
 *
 * @param[in]   stuct thread_args       pass structure
 * 
 */

app_ret_t server_handler(char *message, struct thread_args args);

/**
 * @brief This functon is receive mesage and check authentication.
 * 
 * This function is copy username and password then compare with database.
 * and update fd and status in database. 
 *
 * @param[in]   *msg                pass message
 *
 * @param[in]   stuct thread_args       pass structure
 * 
 */
app_ret_t login_req_handler(char *msg, struct thread_args args);



