/************************
 * Include
************************/
#include "client.h"

/************************
 * Structure
************************/
static struct option long_options[] = {
        {"username", required_argument, 0,  'u' },
        {"password", required_argument, 0,  'p' },
        {"help",     no_argument,       0,  'h' },
        {0,          0,                 0,   0  }
};

app_ret_t command_line_parsing(int argc, char *argv[], char *username, char *password){

	/** Static variable Declaration */
	static int option;
	static int  help = -1;

	while((option = getopt_long(argc, argv,"u:p:h", long_options, NULL)) != -1){
		switch(option){
                        case 'u':
				/** Copy optarg to username*/
				memcpy(username, optarg, MAX_USERNAME_SIZE);
				break;

                        case 'p':
				/** Copy optarg to password*/
				memcpy(password, optarg, MAX_USERNAME_SIZE);
				break;
			case 'h':
				help = 0;
				break;

                        default:
                                printf("Select Proper character !!\n");
                                break;
                }
	}
	if(help == 0){
		printf("HELP !! \n");	
	}

        return NO_ERROR;
}
