/************************
 * Include
************************/
#include"common.h"

/************************
 * Function Prototype
************************/
/** 
 * @brief This function handle signal.
 *
 * This handle software interrupt of application.
 *
 * @param[in]	signo			signal number or signal value 
 *
 */
void sig_handler(int signo);

/**
 * @brief This function parse our argunment which is given run time.
 *
 * This function take argunment and then compare with getopt function for that
 * user switch case and then return individual argunment.
 *
 * @param[in]	argc			Number of argunment
 *
 * @param[in]	**argv			Vector of argunment
 *
 * @param[out]	*username		return username
 *
 * @param[out]	*password		return password
 *
 */
app_ret_t command_line_parsing(int argc, char *argv[], char *username , char *password);

/**
 * @brief This function pass frame of usrname and password.
 *
 * This function take username and password and then copy to ine frame 
 * then send that frame to server.
 *
 * @param[in]	*username		Pass username
 *
 * @param[in]	*password		Pass password
 *
 * @param[in]	*fd			Pass fd for sending
 *
 * @param[in]	sockaddr_in *client	Pass sockaddress for sending
 *
 */
app_ret_t login_request(char *username, char *password, int *sock_fd, struct  sockaddr_in *client);

/**
 * @brief This function is handle client message.
 * 
 * This Function receive message and then check header of frame and then select
 * related case from switch case and print message on client console and if not 
 * match then goes to default. If login sucessfully and one userdefined console 
 * comes and communication start.
 *
 * @param[in]	*message		user message
 *
 * @param[in]	*sock_desc		pass fd
 *
 * @param[in]	sockaddr_in *client	pass sockaddress
 */  
app_ret_t client_handler(char *message, int *sock_desc, struct sockaddr_in *client);

/**
 * @brief This function is Create one promt.
 * 
 * This Function is create one thread and then copy fd, socket address and message
 * in structure.
 *
 * @param[in]   *message                Pass data for sendind
 *
 * @param[in]   *sock_desc              pass fd 
 *
 * @param[in]   sockaddr_in *client     pass socket address     
 *
 */
app_ret_t user_console(char *message, int *sock_desc, struct sockaddr_in *client);

/**
 * @brief This function print message.
 *
 * This message print message on console at client side.
 *
 * @param[in]	*message		pass message
 *
 */
app_ret_t display_message(char *message);


