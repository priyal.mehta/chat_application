/************************
 * Include
************************/
#include<stdio.h>
#include "client.h"

/************************
 * Define
************************/
#define MAX_SIZE_OF_CMD 10
#define PROMPT_COMMAND "\nprompt$"

/************************
 * Structure Declaration
************************/
struct args_t{
	int new_sock_desc;
        struct sockaddr_in client;
	char *message;
};

/************************
 * Structure Declaration
************************/
typedef struct cmd_info{
        char cmd_name[MAX_SIZE_OF_CMD];
        app_ret_t (*cmd_handler)(void *message);
        char cmd_format[MAX_MESSAGE_SIZE];
}cmd_info_t;

/************************
 * Function Prototype
************************/
/**
 * @brief This function is Create one promt.
 * 
 * This Function is create one promt for user communication. In that scan data 
 * from terminal and check command if available then call function else message 
 * print. 
 *
 * @param[in]   *args               	Pass structure which include fd, socket address
 *					and message
 */
void *console_handler(struct args_t *args);

/**
 * @brief This function is Create one promt.
 * 
 * This Function is create one thread and then copy fd, socket address and message
 * in structure.
 *
 * @param[in]	*message		Pass data for sendind
 *
 * @param[in]	*sock_desc		pass fd 
 *
 * @param[in]	sockaddr_in *client	pass socket address	
 *
 */
app_ret_t user_console(char *message, int *sock_desc, struct sockaddr_in *client);

/**
 * @brief This Function Call when user want to send message.
 *
 * This function call then pass Structure and then create frame which include 
 * destination username and message and send to server.
 *
 * @param[in]   *args                   Pass structure which include fd, socket address
 *                                      and message
 */
app_ret_t do_send(struct args_t *args);

/**
 * @brief This Function Call when user want to help.
 *
 * This function call when user want to see the frame fromate and which commands
 * are available.
 *
 */
app_ret_t do_help();

/**
 * @brief This Function Call when user want to logout.
 *
 * This function call then pass Structure and then send message to server for logout.
 * And then Exit client console.
 *
 * @param[in]   *args                   Pass structure which include fd, socket address
 *                                      and message
 */
app_ret_t do_logout(struct args_t *args);

