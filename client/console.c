/************************
 * Include
************************/
#include"console.h"

/************************
 * define
************************/
#define MAX_SUPPORTED_CMD ((sizeof(cmd_info)/ sizeof(cmd_info_t)))

/************************
 * Static declartion
************************/
static cmd_info_t cmd_info[] = {
                        {"send",do_send, "send destination_username message"},
                        {"logout",do_logout, "logout"},
                        {"help",do_help, "help"},
                };


app_ret_t user_console(char *message, int *sock_desc, struct sockaddr_in *client){

	/** Variable Declaration */
        pthread_t thread1;
	struct args_t *args1;

	/** Memory Allocation */
	args1 = malloc(sizeof(struct args_t));
	if(args1 == NULL){
		debug_print("Error in memory allocation !1\n");
		return NULL_POINTER;
	}

	/** copy data in structure */
	args1->new_sock_desc = *sock_desc;
	args1->message  = message;
	args1->client = *client;

	/** thread Creation */
        if(pthread_create(&thread1, NULL, console_handler,(void *)args1) < 0){
                debug_print("Error in thread create !!\n");
                return FAIL;
        }
        return NO_ERROR;
}

void *console_handler(struct args_t *args){

	/** Variable Declaration */
        int i, index, length, flag = 0;
        char cmd[MAX_MESSAGE_SIZE];
	char test[MAX_USERNAME_SIZE];

        while(1){
                i = 0;

		/** Fill memory with constant zero */
		memset(args->message, 0, MAX_MESSAGE_SIZE);
		memset(cmd, 0, MAX_MESSAGE_SIZE);

                printf("%s",PROMPT_COMMAND);

		/** Scan data from terminal */
                fgets(args->message, MAX_MESSAGE_SIZE , stdin);

		/** get string upto space */
                while(args->message[i] != ' '){
                        cmd[i] = args->message[i];
                        i++;
                }
		i++;
                cmd[i] = '\0';

                for(index = 0; index < MAX_SUPPORTED_CMD; index++){
			length = 0;
			length = strlen(cmd_info[index].cmd_name);
			
			/** Check command compare with structure */
			if(strncmp(cmd_info[index].cmd_name, cmd, length) == 0){
				flag = 1;
				/** Call particular function as per command */
				cmd_info[index].cmd_handler((struct args_t *)args);	
			}
                }
	
		/** If command not match then print*/
		if(flag == 0){
			printf("Command not found : %s\n", cmd);
			printf("Availble commands\nsend\nhelp\nlogout\n");
		}
	}	
}

app_ret_t do_send(struct args_t *args){

	/** Variable declaration */
	char message[MAX_MESSAGE_SIZE] = {0};
	int i = 0 ;
	app_ret_t ret;

	while(args->message[i] != ' '){
		i++;
        }
	i++;

	/** Fill message with header */
	message[HEADER_CMD_INDEX] = MSG;

	/** Copy destination username */
	memcpy((message + MSG_DEST_USERNAME_START), ((args->message) + i), MAX_USERNAME_SIZE);

	while(args->message[i] != ' '){
		i++;
        }
	i++;

	/** Copy message */
	memcpy((message + MSG_DATA), ((args->message) + i), (MAX_MESSAGE_SIZE - MAX_USERNAME_SIZE - HEADER_CMD_SIZE));

	/** Send message */
	ret = socket_send(&(args->new_sock_desc),message,(&(args->client)));
        if(ret == ERR_SENDING_MSG){
		debug_print("Error in sending message !!\n");
                return FAIL;
	}
	return NO_ERROR;
}

app_ret_t do_help(){

	/** Variable Declaration */
	int index;

	/** Print command with command formate */
	for(index = 0; index < MAX_SUPPORTED_CMD; index++){
		printf("%s\n",cmd_info[index].cmd_format);
	}
	return NO_ERROR;
}

app_ret_t do_logout(struct args_t *args){
	printf("LOGOUT SUCESSFULLY !!\n");

	/** Variable Declaration */
	char msg[MAX_MESSAGE_SIZE] = "Thank you !!\n";
	char message[MAX_MESSAGE_SIZE];
	app_ret_t ret;

	/** Fill message header */
	message[HEADER_CMD_INDEX] = LOGOUT_REQ;

	/** Copy message */
	memcpy((message + MSG_START), msg, MAX_MESSAGE_SIZE - HEADER_CMD_SIZE);

	/** Send socket */
	ret = socket_send(&(args->new_sock_desc),message,(&(args->client)));
        if(ret == ERR_SENDING_MSG){
                debug_print("Error in sending message !!\n");
                return FAIL;
        }
	exit(0);
}

