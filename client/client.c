/************************
 * Include
************************/
#include "client.h"

app_ret_t login_request(char *username, char *password, int *sock_desc, struct sockaddr_in *server){

	/** Variable declaration */
	int ulen, plen;
	app_ret_t ret;
	char frame[MAX_MESSAGE_SIZE];

	/** Fill memory with constant zero */
	memset(frame, 0, MAX_MESSAGE_SIZE);

	/** check mmory Assignment */
	if(username == NULL  || password == NULL){
                printf("Enter Username and password !!\n");
                return NULL_POINTER;
        }

	/** Get length of username and password */
        ulen = strlen(username);
        plen = strlen(password);

        if(ulen !=  MAX_USERNAME_SIZE  || plen != MAX_PASSWORD_SIZE){
                printf("Size of username and password is incorrect !!\n");
                return ERR_INVALID_LOGIN_DETAILS;
        }

	/** Fill Header command in frame */
	frame[HEADER_CMD_INDEX] = LOGIN_REQ;
	memcpy(frame + LOGIN_REQ_USERNAME_START, username, MAX_USERNAME_SIZE);

	memcpy(frame + LOGIN_REQ_PASSWORD_START, password, MAX_PASSWORD_SIZE);

	/** Send frame */
	ret = socket_send(sock_desc,frame, server);
        if(ret == FAIL){
                debug_print("Error in sending message with ret val : %d\n" ,ret);
                return FAIL;
        }
	return NO_ERROR;
}

app_ret_t client_handler(char *message, int *sock_desc, struct sockaddr_in *server){

	/** Variable declaration */
        app_ret_t ret;
	app_frame_type check;
	login_response_t check1;

	/** check Message of header */
        check = message[HEADER_CMD_INDEX];
        switch(check){	
		case LOGIN_RESPONSE:
			
			/** check Message of header */
			check1 = message[MESSAGE_RESPONSE];

			switch(check1){
		                case LOGIN_RESPONSE_SUCESSFULLY_LOGIN:
					printf("Successfully login !!\n");
					ret = user_console(message, sock_desc, server);
				        if(ret != NO_ERROR){
				                debug_print("Error in message receiving  !!\n");
				                return FAIL;
				        }		
				        break;

		                case LOGIN_RESPONSE_USERNAME_INVALID:
        		                printf("Username Invalid !!\n");
                		        break;

	                	case LOGIN_RESPONSE_PASSWORD_INVALID:
        	                	printf("Password Invalid !!\n");
	                	        break;

		                case LOGIN_RESPONSE_USER_EXISTS:
        		                printf("User already exists !!\n");
					exit(0);
		
				default:
					printf("Invalid Login response !!\n");
					break;
        		}
			break;
		case MSG : 
			ret = display_message(message);
			if(ret != NO_ERROR){
				debug_print("Error in dispaly message !!\n");
				return FAIL;
			}
			break;
		
		default:
			printf("Invalid message frame !!\n");
			break;
	}
        return NO_ERROR;
}

app_ret_t display_message(char *message){
	/** Variable declaration */
	int index;

	/** Print message */
	for(index = MESSAGE_RESPONSE; index < strlen(message); index++){
		printf("%c",message[index]);
        }
	message[index]= '\0';
        printf("\n");

	return NO_ERROR;
}

