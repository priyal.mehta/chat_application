/*******************************************************************************
 * Copyright(c) 2017, Volansys Technologies                                    *
 *                                                                             *
 * This is a chat_client file.                                                 * 
 *                                                                             *
 * This is a chat_client file which will call the APIS from client side.       *
 * The API is called in this file and function will search defination from     *
 * client.h.                                                                   *
 ******************************************************************************/

/**
 * @file chat_client.c
 * @author Priyal Mehta
 * @date 09/11/2017
 * @brief
 *
 * chat_client file which called client APIS.
 *
 * This file allowed UDP and TCP connection which will use to communication between 
 * server and client. The features of the API is login request, logout request, send,
 * help. First initilize socket and then user login. After login Communication is 
 * Established.
 *
 */

/************************
 * Include
************************/
#include "client.h"
#include "header.h"

/************************
 * Globle variable
************************/
static int fd = 0;
struct sockaddr_in client;

/***********************
 * Function Prototypes
 ***********************/
/**
 * @brief This is a main function.
 *
 * The function main() is a main function of file. main() indicate complier to
 * start program execution from main(). Here main function is designed to use
 * different APIs from library  which is establish the connetion. main() will
 * first initilize socket and then parse username and password and send to server
 * and and server check the database and if is in then change status and store fd.
 * Also server send acknowledgement to client(i.e. Login sucessfully or not).
 * Then new Promt open and start the communication client to client throught server.
 *
 */

int main(int argc, char *argv[]){

	/** Variable initilization and memory Allocation */
	char *username = (char *)malloc(MAX_USERNAME_SIZE *sizeof(char));
	char *password = (char *)malloc(MAX_PASSWORD_SIZE *sizeof(char)); 
	char message[MAX_MESSAGE_SIZE];
	app_ret_t ret;
	struct sockaddr_in server;

	/** Fill memory with constant zero. */
	memset(&client,0,sizeof(client));
	memset(&server,0,sizeof(server));

	/** Initilize signal if client want to close */
	signal(SIGINT, sig_handler);

	/** Socket initilization */
	ret = socket_client_init(&server, &fd);
        if(ret != NO_ERROR){
                debug_print("Error in connection request with ret val : %d\n", ret);
                return FAIL;
	}

	/** Parse command line */
	ret = command_line_parsing(argc, argv, username, password);
	if(ret != NO_ERROR){
                debug_print("Error in command line parsing with ret val : %d\n", ret);
                return FAIL;
	}

	/** Login request frame send to server */
	ret = login_request(username, password, &fd, &client);
	if(ret !=  NO_ERROR){
                debug_print("Error in authentication request with ret val : %d\n", ret);
                return FAIL;
        }

	while(1){
		/** Fill memory with constant zero */
		memset(message, 0, MAX_MESSAGE_SIZE);

		/** receive message */
		ret = socket_receive(&fd, message, &client);
	        if(ret == FAIL){
        	        debug_print("Error in receiving message with ret val : %d\n", ret);
                	return FAIL;
        	}	

		/** Client handle the message */
		ret = client_handler(message, &fd, &client);
		if(ret != NO_ERROR){
                        debug_print("Error in client receiver message with ret val : %d\n", ret);
                        return FAIL;
                }

        }
	/** close fd */
	close(fd);
	return NO_ERROR;
}

void sig_handler(int signo){

	/** Varible initilization and declaration */
        int ret = 0;
	char message[MAX_MESSAGE_SIZE];

	/** Fill frame type in frame */
        message[0] = LOGOUT_REQ;
        if (signo == SIGINT){
                printf("Thank You !!\n");

		/** Send message to server */
		ret = socket_send(&fd, message, &client);
                if(ret == FAIL){
                        debug_print("Error in sending message with ret val: %d\n", ret);
                }
		exit(0);
        }
}

